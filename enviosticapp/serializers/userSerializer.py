from rest_framework import fields, serializers
from enviosticApp.models.user import User
from enviosticApp.models.shipping import Shipping
from enviosticApp.serializers.shippingSerializer import ShippingSerializer

class UserSerializers(serializers.ModelSerializer):
    shipping = ShippingSerializer()
    class Meta:
        model = User
        fields = ['id', 'user', 'address', 'phone', 'email', 'shipping']

    def create(self, validated_data):
        shippindData = validated_data.pop('shipping')
        userInstance = User.objects.create(**validated_data)
        Shipping.objects.create(user = userInstance, **shippindData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id = obj.id)
        shipping = Shipping.objects.get(id = obj.id)
        return {
                 'id': user.id,
                 'user': user.user,
                 'address': user.address,
                 'phone': user.phone,
                 'email': user.email,
                 'shipping':{
                     'tracking_guide': shipping.tracking_guide,
                     'destination': shipping.destination,
                     'weight': shipping.weight,
                     'time': shipping.time,
                     'status': shipping.status,    
                 
                 
                 }
        
        }