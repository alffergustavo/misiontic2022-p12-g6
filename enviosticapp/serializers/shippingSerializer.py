from  enviosticApp.models.shipping import Shipping
from rest_framework import serializers

class ShippingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipping
        fields = ['tracking_guide', 'destination', 'weight', 'time','status']