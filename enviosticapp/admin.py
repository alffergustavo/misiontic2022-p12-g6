from django.contrib import admin
from .models.shipping import Shipping
from .models.user import User

# Register your models here.

admin.site.register(User)
admin.site.register(Shipping)

