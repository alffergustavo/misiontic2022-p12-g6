from django.db import models
from .user import User
from django.db.models.fields.related import ForeignKey    
    
class Shipping(models.Model):
    
    id_user=  models.ForeignKey(User, on_delete= models.CASCADE)
    tracking_guide = models.IntegerField(primary_key=True, unique=True)
    destination= models.CharField(max_length=40)
    weight=  models.IntegerField(default=0)
    time= models.DateTimeField()
    status= models.CharField(max_length=20)