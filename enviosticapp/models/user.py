from django.db import models

class User(models.Model):
    id= models.IntegerField(primary_key= True, unique=True)
    user=  models.CharField(max_length=50)
    address= models.CharField(max_length=80)
    phone= models.IntegerField(default=0)
    email= models.EmailField(max_length=100)