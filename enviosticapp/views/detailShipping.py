from django.shortcuts import render
from enviosticApp.serializers import ShippingSerializer
from enviosticApp.models import Shipping
from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

class DetailShipping(APIView):

    def get_object(self, pk):
        try:
            return Shipping.objects.get(pk=pk)
        except Shipping.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        shipping = self.get_object(pk)
        serializer = ShippingSerializer(shipping)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        shipping = self.get_object(pk)
        serializer = ShippingSerializer(shipping, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        shipping= self.get_object(pk)
        shipping.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
