from django.shortcuts import render
from enviosticApp.models import Shipping
from enviosticApp.serializers import ShippingSerializer
from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

class CreateShipping(APIView):
    
    def get(self, request, format=None):
        shipping = Shipping.objects.all()
        serializer = ShippingSerializer(shipping, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):        
        serializer =  ShippingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
