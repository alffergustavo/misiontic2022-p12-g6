from django.shortcuts import render
from enviosticApp.models import User
from enviosticApp.serializers import UserSerializers
from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

class CreateUser(APIView):
    
    def get(self, request, format=None):
        user = User.objects.all()
        serializer = UserSerializers(user, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):        
        serializer =  UserSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
